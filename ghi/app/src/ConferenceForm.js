import React, { useEffect, useState } from 'react';

function ConferenceForm () {

    const [name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [starts, setStarts] = useState('')
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('')
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const [description, setDescription] = useState('')
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [maxpres, setMaxPres] = useState('')
    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPres(value);
    }

    const [maxatt, setMaxAtt] = useState('')
    const handleMaxAttChange = (event) => {
        const value = event.target.value;
        setMaxAtt(value);
    }

    const [location, setLocation] = useState('')
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    useEffect(  () =>  {
        fetchData()

    }, [] );
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)

        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxpres;
        data.max_attendees = maxatt;
        data.location = location
        console.log(data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();

          setName('')
          setStarts('')
          setEnds('')
          setDescription('')
          setMaxPres('')
          setMaxAtt('')
          setLocation('')

        }

    }


    return (
        <div className="row">
         <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a New Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">

              <div className="form-floating mb-3">
                <input
                placeholder="Name"
                name="name"
                required type="text"
                id="name"
                className="form-control"
                onChange={handleNameChange}
                value={name}/>
                <label htmlFor="name">Name</label>
              </div>



              <div className="form-floating mb-3">
                <input
                placeholder="mm/dd/yyy"
                name="starts" required
                type="date" id="starts"
                className="form-control"
                onChange={handleStartsChange}
                value={starts} />
                <label htmlFor="starts">Start Date:</label>
              </div>

              <div className="form-floating mb-3">
                <input
                placeholder="mm/dd/yyy"
                name="ends"
                required type="date"
                id="ends"
                className="form-control"
                onChange={handleEndsChange}
                value={ends} />
                <label htmlFor="ends">End Date:</label>
              </div>

              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea
                className="form-control"
                id="descrption"
                rows="3"
                onChange={handleDescriptionChange}
                value={description} ></textarea>
              </div>

              <div className="form-floating mb-3">
                <input
                placeholder="Maximum Presentations"
                name="max_presentations"
                required type="number"
                id="max_presentations"
                className="form-control"
                onChange={handleMaxPresChange}
                value={maxpres} />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>

              <div className="form-floating mb-3">
                <input
                placeholder="Maximum Attendees"
                name="max_attendees"
                required type="number"
                id="max_attendees"
                className="form-control"
                onChange={handleMaxAttChange}
                value={maxatt} />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>

              <div className="mb-3">
                <select
                required id="location"
                name="location"
                className="form-select"
                onChange={handleLocationChange}
                value={location}>
                  <option value="">Choose a Location</option>
                  {locations.map (location => {
                    return (
                        <option value={location.id} key={location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>

              <button className="btn btn-primary">Create</button>
             </form>

        </div>
        </div>
        </div>




    );
}




export default ConferenceForm ;
