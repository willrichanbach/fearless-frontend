
function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer text-muted">
      ${startDate}-${endDate}
      </div>
    </div>
    `;
}



window.addEventListener('DOMContentLoaded', async () => {



    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col')
    let colIndx = 0

    try {
        const response = await fetch(url);

        if (!response.ok){
            // if response bad do this
            const errorMessage = `
            <div class="alert alert-primary" role="alert">
            We were not able to load data at this time.
            </div>
            `
            const column = columns[0];
            column.innerHTML = errorMessage

        } else {
            const data = await response.json();

            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const isoStartDate = details.conference.starts
                    const isoEndDate = details.conference.ends
                    const startDate = new Date(isoStartDate).toLocaleDateString()
                    const endDate = new Date(isoEndDate).toLocaleDateString()
                    const location = details.conference.location.name

                    const html = createCard(name, description, pictureUrl, startDate, endDate, location);
                    const column = columns[colIndx % 3];
                    column.innerHTML += html;
                    colIndx = (colIndx + 1) % 3

                }

            }


        }
    } catch (e) {
        console.error(e)
        // add error handling
    }

});
