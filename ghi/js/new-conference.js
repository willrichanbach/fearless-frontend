


window.addEventListener('DOMContentLoaded', async () => {


    try {
        const url = 'http://localhost:8000/api/locations/';
        const locationFetch = await fetch(url);
        if (locationFetch.ok){
            const locationData = await locationFetch.json();
            const selector = document.getElementById("location");
            for (let location of locationData.locations){
                const newOption = document.createElement("option");
                newOption.value = location.id;
                newOption.innerHTML = location.name;
                selector.appendChild(newOption);
            };

        } else {
            console.log("Fetch Failed");
        }

        const formTag = document.getElementById("create-conference-form");

        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag)
            const jsonData = JSON.stringify(Object.fromEntries(formData));
            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: jsonData,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok){
                formTag.reset();
                const newConference = await response.json();
            }

        });


    } catch(e) {
        console.error(e);
        // add error handling
    };

});
