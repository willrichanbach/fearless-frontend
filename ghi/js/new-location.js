


window.addEventListener('DOMContentLoaded', async () => {


    try {
        const url = 'http://localhost:8000/api/states/';
        const stateFetch = await fetch(url);
        if (stateFetch.ok){
            const stateData = await stateFetch.json();
            console.log(stateData);
            const selector = document.getElementById("state");
            for (let state of stateData.states){
                console.log(state);
                const newOption = document.createElement("option");
                newOption.value = state.abbreviation;
                newOption.innerHTML = state.name;
                selector.appendChild(newOption);
            };

        } else {
            console.log("Fetch Failed");
        }

        const formTag = document.getElementById("create-location-form");

        formTag.addEventListener('submit', async (event) => {
            event.preventDefault();
            const formData = new FormData(formTag)
            const jsonData = JSON.stringify(Object.fromEntries(formData));
            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: jsonData,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok){
                formTag.reset();
                const newLocation = await response.json();
            }

        });


    } catch(e) {
        console.error(e);
        // add error handling
    };

});
